<p align="center">
	<a href="https://kalvan.store" target="_blank"><img src="https://note.youdao.com/yws/api/personal/file/WEBa0c10947e95e9e5adf89f914735a8a4b?method=download&shareKey=7759b0f54e90123a71101088dbbceda1" ></a>
</p>
<p align="center">
	<strong>精简·极致</strong>
</p>
<p align="center">
	<a target="_blank" href="https://search.maven.org/search?q=store.kalvan">
        <img src="https://img.shields.io/maven-central/v/store.kalvan.framework/kalvan-core.svg?label=Maven%20Central" />
	</a>
	<a target="_blank" href="https://baike.baidu.com/item/MIT%E8%AE%B8%E5%8F%AF%E8%AF%81/6671281?fr=aladdin">
        <img src="https://img.shields.io/:license-MIT-blue.svg" />
	</a>
	<a target="_blank" href="https://www.oracle.com/technetwork/java/javase/downloads/index.html">
		<img src="https://img.shields.io/badge/JDK-8+-green.svg" />
	</a>
	<a target="_blank" href="https://gitee.com/spring_thunder/kalvan-admin/stargazers">
		<img src="https://gitee.com/spring_thunder/kalvan-admin/badge/star.svg?theme=dark" alt='gitee star'/>
	</a>
</p>
<p align="center">
	<a href="https://kalvan.store" target="_blank">KV平台</a> 
	<a href="https://kalvan.store/kalvan-admin"  target="_blank">在线演示</a> 
	<a href="https://kalvan.store/docs/learn/install-env-cn"  target="_blank">在线文档</a>
	<a href="https://kalvan.store/docs/learn/kalvan-gen-cn" target="_blank">代码生成器视频教程</a><br/>
	<a href="https://kalvan.store" target="_blank">腾讯课堂在线视频</a><br/>
</p>

-------------------------------------------------------------------------------
