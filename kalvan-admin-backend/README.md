* [适应场景](#适应场景)
* [快速使用](#快速使用)
* [后端开发](#后端开发)
  * [核心结构](#核心结构)
  * [业务代码结构](#业务代码结构)
  * [resource结构](#resource结构)

# 前置条件
> 初始化数据库 [kalvan-demo](https://gitee.com/spring_thunder/kalvan-demo)  `db`  文件夹下的脚本

> 依赖模块安装 [kalvan-demo](https://gitee.com/spring_thunder/kalvan-demo) 中的模块

> 强依赖`kalvan-demo` 下的`scc` 模块

> 依赖相关 [中间件](https://kalvan.store/docs/learn/install-env-cn)

# 适应场景
>适用于需要登录、管理界面、权限控制的web项目
>
>可构建管理系统、商户系统、代理商等后台系统

# 快速使用
1. 下载前端工程[kalvan-admin-frontend](../kalvan-admin-frontend/README.md)
2. 下载idea插件[kalvan-gen](https://plugins.jetbrains.com/plugin/15002-kalvan-gen)
3. 参照官网文档运行[kalvan-admin](https://kalvan.store/docs/learn/kalvan-admin-cn)

# 后端开发
## 核心结构
```
com.kalvan
|- admin   
    |-system 系统管理模块
|- web 
    |- dict 字典缓存工作类
    |- email 邮件工具类
    |- excel 导入导出工具类
    |- exception 统一异常处理拦截
    |- log 日志记录Aop
    |- servlet web控制类
```
**统一请求参数转换处理**
```
com.kalvan.web.servlet.request
```
**统一日志链路跟踪**
```
com.kalvan.web.servlet.interceptor.CallInterfaceInterceptor
```
**统一登录状态检查**
```
com.kalvan.web.servlet.interceptor.TokenInterceptor
```
**统一权限检查**
```
com.kalvan.web.servlet.interceptor.PermissionInterceptor
```
**统一请求参数加解密**
```
com.kalvan.web.servlet.interceptor.DecryptInterceptor
```
**统一请求参数检查(分组控制)**
```
@Validated
```
**统一异常返回**
```
com.kalvan.web.exception.ExceptionHandle
```
**统一返回结果转换处理**
```
com.kalvan.web.servlet.response
```

## 业务代码结构
```
|-- commons                                     //公共包
|   |-- constant                                //常量包
|   |    |--DictType.java                       //字典常量定义
|   |    |--LogType.java                        //操作日志常量定义
|   |-- db                                      //orm包
|   |    |--库名A                                //数据库名
|   |    |  |--mapper                           //mapper接口
|   |    |  |--model                            //数据库实体对象
|   |    |  |--库名ADataSourceConfig.java        //数据源和事务配置
|   |    |--DataSourceConstants.java            //数据源配置常量定义包含xml路径,命名
|   |-- dto 
|   |    |--库名A                                //存放需要增加或者导入的DTO对象按库来分
|-- 子系统S                                      //子系统包
|   |-- 模块M                                    //子模块包
|   |    |--controller                          //请求接收控制层
|   |    |--service                             //业务逻辑处理层
|-- web                                         //shiro配置

```
## resource结构
```
|-- db                                      //orm包
|    |--库名A                                //数据库名
|    |  |--mapper                           //mapper xml sql
|    |  |--A-sharding.yml                   //sharding-jdbc数据源配置文件
|-- kv                                      //插件生成目录
|-- static                                  //静态资源目录
|    |--template                            //文件导入模板目录

```
