package com.kalvan.admin.gateway.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.extra.validation.BeanValidationResult;
import cn.hutool.extra.validation.ValidationUtil;
import com.demo.enums.cache.CacheTypeEnum;
import com.demo.enums.mq.MqTopicEnum;
import com.github.pagehelper.Page;
import com.kalvan.admin.annotation.ParamsDecrypted;
import com.kalvan.admin.annotation.Permission;
import com.kalvan.admin.annotation.SysLog;
import com.kalvan.admin.excel.ExcelUtil;
import com.kalvan.admin.gateway.service.DictService;
import com.kalvan.admin.log.LogType;
import com.kalvan.admin.valid.Add;
import com.kalvan.admin.valid.Edit;
import com.kalvan.client.constant.CommonReturnCode;
import com.kalvan.client.exception.BizException;
import com.kalvan.client.model.WebResponse;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.constant.LogGroup;
import com.kalvan.commons.db.gateway.mapper.DictMapper;
import com.kalvan.commons.db.gateway.model.Dict;
import com.kalvan.commons.dto.gateway.DictImport;
import com.kalvan.db.mybatis.PageInfo;
import com.kalvan.mq.MqUtil;
import com.kalvan.web.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 数据字典配置Controller
 *
 * @author kalvan
 * @date 2020-08-23 15:07:36
 */
@SysLog(group = LogGroup.DICT)
@RestController
@RequestMapping("gateway/dict")
@Slf4j
public class DictController extends BaseController {
    private static final int DOWNLOAD_MAX_COUNT = 20000;
    @Resource
    DictService dictService;
    @Resource
    DictMapper dictMapper;

    /**
     * 分页查询
     *
     * @param params 查询参数
     * @return WebResponse
     */
    @PostMapping("list")
    @Permission("admin:gateway:dict:list")
    public WebResponse list(PageInfo pageInfo, @RequestParam Map<String, Object> params) {
        Page data = dictService.queryPage(pageInfo, params);
        return WebResponse.buildSuccess().putPage(data);
    }

    /**
     * 根据主键查询详情
     *
     * @param id 主键
     * @return WebResponse
     */
    @GetMapping("info/{id}")
    @Permission("admin:gateway:dict:info")
    public WebResponse info(@PathVariable("id") Long id) {
        Dict dict = dictService.findById(id);
        if (dict == null) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        return WebResponse.buildSuccess().putData(dict);
    }

    /**
     * 下载
     *
     * @param params   查询参数
     * @param response response
     * @throws Exception 抛出异常拦截器统一处理
     */
    @SysLog(remark = "导出字典信息", type = LogType.EXPORT)
    @PostMapping("download")
    @Permission("admin:gateway:dict:download")
    public void download(@RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        PageInfo pageInfo = new PageInfo();
        pageInfo.setPage(1);
        pageInfo.setLimit(DOWNLOAD_MAX_COUNT);
        Page data = dictService.queryPage(pageInfo, params);
        String filename = "数据字典配置";
        if (data.getTotal() > DOWNLOAD_MAX_COUNT) {
            throw new BizException(AdminReturnCode.PARAM_REGION_OVER.formatDesc("下载条数", DOWNLOAD_MAX_COUNT));
        }
        filename = filename + DatePattern.PURE_DATETIME_MS_FORMAT.format(DateUtil.date());
        ExcelUtil.download(filename, response, Dict.class, data);
    }

    /**
     * 新增数据字典配置
     *
     * @param dictImport dictImport
     * @return WebResponse
     */
    @SysLog(remark = "新增数据字典配置", type = LogType.ADD)
    @PostMapping("add")
    @Permission("admin:gateway:dict:add")
    public WebResponse add(@Validated({Add.class}) DictImport dictImport) {
        dictService.add(BeanUtil.toBean(dictImport, Dict.class));
        return WebResponse.buildSuccess();
    }

    /**
     * 数据字典配置导入模板下载
     *
     * @param response response
     * @throws Exception 抛出异常拦截器统一处理
     */
    @GetMapping("downloadTemplate")
    @Permission("admin:gateway:dict:upload")
    public void downloadTemplate(HttpServletResponse response) throws Exception {
        ExcelUtil.downloadTemplate(templateRootPath + "com/kalvan/admin/数据字典配置-模板.xls", response);
    }

    /**
     * 导入数据字典配置
     *
     * @param file file
     * @return WebResponse
     */
    @SysLog(remark = "导入数据字典配置", type = LogType.IMPORT)
    @PostMapping("upload")
    @Permission("admin:gateway:dict:upload")
    @ParamsDecrypted(required = false)
    public WebResponse upload(@RequestParam MultipartFile file) throws Exception {
        List<DictImport> importList = ExcelUtil.importExcel(file, 1, 1, DictImport.class);
        for (DictImport dictImport : importList) {
            BeanValidationResult validate = ValidationUtil.warpValidate(dictImport, Add.class);
            if (!validate.isSuccess()) {
                throw new BizException(CommonReturnCode.PARAM_ILLEGAL.formatDesc(validate.getErrorMessages().stream().map(BeanValidationResult.ErrorMessage::getMessage).collect(Collectors.joining(","))));
            }
            dictService.add(BeanUtil.toBean(dictImport, Dict.class));
        }
        return WebResponse.buildSuccess(AdminReturnCode.SUCCESS_IMPORT.getFormatDesc(importList.size()));
    }

    /**
     * 修改数据字典配置
     *
     * @param dictImport dictImport
     * @return WebResponse
     */
    @SysLog(remark = "修改数据字典配置", type = LogType.EDIT)
    @PostMapping("edit")
    @Permission("admin:gateway:dict:edit")
    public WebResponse edit(@Validated({Edit.class}) DictImport dictImport) {
        if (dictImport.getId() == null) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        dictService.edit(BeanUtil.toBean(dictImport, Dict.class));
        return WebResponse.buildSuccess();
    }

    /**
     * 根据主键删除数据字典配置
     *
     * @param ids ids
     * @return WebResponse
     */
    @SysLog(remark = "删除数据字典配置", type = LogType.DELETE)
    @PostMapping("delete")
    @Permission("admin:gateway:dict:delete")
    public WebResponse delete(Integer[] ids) {
        if (ArrayUtils.isEmpty(ids)) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        int num = dictService.deleteBatch(ids);
        return WebResponse.buildSuccess(AdminReturnCode.SUCCESS_DELETE.getFormatDesc(num));
    }

    @Permission("admin:gateway:dict:updateCache")
    @PostMapping("updateCache")
    public WebResponse updateCache(String dictType) {
        log.info("广播通知更新字典缓存");
        //TODO 批量更新jetcache缓存需要实现
        MqUtil.getProducer().sendAsyncMessage(MqTopicEnum.TOPIC_CACHE_REFRESH,
                CacheTypeEnum.DICT, System.currentTimeMillis() + "", dictType == null ? "" : dictType);
        return WebResponse.buildSuccess();
    }

    @PostMapping("allType")
    public WebResponse allType() {
        return WebResponse.buildSuccess().putData(dictMapper.selectAllType());
    }

}
