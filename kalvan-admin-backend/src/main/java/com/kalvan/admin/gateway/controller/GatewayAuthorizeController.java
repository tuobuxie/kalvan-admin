package com.kalvan.admin.gateway.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.extra.validation.BeanValidationResult;
import cn.hutool.extra.validation.ValidationUtil;
import com.alibaba.fastjson.JSON;
import com.demo.enums.cache.CacheTypeEnum;
import com.demo.enums.mq.MqTopicEnum;
import com.demo.enums.mq.msg.GatewayCache;
import com.github.pagehelper.Page;
import com.kalvan.admin.annotation.ParamsDecrypted;
import com.kalvan.admin.annotation.Permission;
import com.kalvan.admin.excel.ExcelUtil;
import com.kalvan.admin.gateway.service.GatewayAuthorizeService;
import com.kalvan.admin.valid.Add;
import com.kalvan.admin.valid.Edit;
import com.kalvan.client.constant.CommonReturnCode;
import com.kalvan.client.context.RequestContextHolder;
import com.kalvan.client.exception.BizException;
import com.kalvan.client.model.WebResponse;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.constant.AdminContextCacheKey;
import com.kalvan.commons.constant.LogGroup;
import com.kalvan.commons.db.gateway.model.GatewayAuthorize;
import com.kalvan.commons.dto.gateway.GatewayAuthorizeImport;
import com.kalvan.admin.log.LogType;
import com.kalvan.admin.annotation.SysLog;
import com.kalvan.db.mybatis.PageInfo;
import com.kalvan.mq.MqUtil;
import com.kalvan.web.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 接入方服务授权表Controller
 *
 * @author kalvan
 */
@SysLog(group = LogGroup.GATEWAY_AUTHORIZE)
@RestController
@RequestMapping("gateway/authorize")
@Slf4j
public class GatewayAuthorizeController extends BaseController {
    @Resource
    GatewayAuthorizeService gatewayAuthorizeService;

    /**
     * 分页查询
     *
     * @param params 查询参数
     * @return WebResponse
     */
    @PostMapping("list")
    @Permission("admin:gateway:authorize:list")
    public WebResponse list(PageInfo pageInfo, @RequestParam Map<String, Object> params) {
        Page<GatewayAuthorize> data = gatewayAuthorizeService.queryPage(pageInfo, params);
        return WebResponse.buildSuccess().putPage(data);
    }

    /**
     * 根据主键查询详情
     *
     * @param id 主键
     * @return WebResponse
     */
    @GetMapping("info/{id}")
    @Permission("admin:gateway:authorize:info")
    public WebResponse info(@PathVariable("id") Long id) {
        GatewayAuthorize gatewayAuthorize = gatewayAuthorizeService.findById(id);
        if (gatewayAuthorize == null) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        return WebResponse.buildSuccess().putData(gatewayAuthorize);
    }

    private static final int DOWNLOAD_MAX_COUNT = 20000;

    /**
     * 下载
     *
     * @param params   查询参数
     * @param response response
     * @throws Exception 抛出异常拦截器统一处理
     */
    @SysLog(remark = "导出接口授权信息", type = LogType.EXPORT)
    @PostMapping("download")
    @Permission("admin:gateway:authorize:download")
    public void download(@RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        PageInfo pageInfo = new PageInfo();
        pageInfo.setPage(1);
        pageInfo.setLimit(DOWNLOAD_MAX_COUNT);
        Page<GatewayAuthorize> data = gatewayAuthorizeService.queryPage(pageInfo, params);
        String filename = "接入方服务授权表";
        if (data.getTotal() > DOWNLOAD_MAX_COUNT) {
            throw new BizException(AdminReturnCode.PARAM_REGION_OVER.formatDesc("下载条数", DOWNLOAD_MAX_COUNT));
        }
        filename = filename + DatePattern.PURE_DATETIME_MS_FORMAT.format(DateUtil.date());
        ExcelUtil.download(filename, response, GatewayAuthorize.class, data);
    }

    /**
     * 新增接入方服务授权表
     *
     * @param gatewayAuthorizeImport gatewayAuthorizeImport
     * @return WebResponse
     */
    @SysLog(remark = "新增接入方服务授权表", type = LogType.ADD)
    @PostMapping("add")
    @Permission("admin:gateway:authorize:add")
    public WebResponse add(@Validated({Add.class}) GatewayAuthorizeImport gatewayAuthorizeImport) {
        gatewayAuthorizeService.add(BeanUtil.toBean(gatewayAuthorizeImport, GatewayAuthorize.class));
        return WebResponse.buildSuccess();
    }

    /**
     * 接入方服务授权表导入模板下载
     *
     * @param response response
     * @throws Exception 抛出异常拦截器统一处理
     */
    @GetMapping("downloadTemplate")
    @Permission("admin:gateway:authorize:upload")
    public void downloadTemplate(HttpServletResponse response) throws Exception {
        ExcelUtil.downloadTemplate(templateRootPath + "com.kalvan/admin/接入方服务授权表-模板.xls", response);
    }

    /**
     * 导入接入方服务授权表
     *
     * @param file file
     * @return WebResponse
     */
    @SysLog(remark = "导入接入方服务授权表", type = LogType.IMPORT)
    @PostMapping("upload")
    @Permission("admin:gateway:authorize:upload")
    @ParamsDecrypted(required = false)
    public WebResponse upload(@RequestParam MultipartFile file) throws Exception {
        List<GatewayAuthorizeImport> importList = ExcelUtil.importExcel(file, 1, 1, GatewayAuthorizeImport.class);
        for (GatewayAuthorizeImport gatewayAuthorizeImport : importList) {
            BeanValidationResult validate = ValidationUtil.warpValidate(gatewayAuthorizeImport, Add.class);
            if (!validate.isSuccess()) {
                throw new BizException(CommonReturnCode.PARAM_ILLEGAL.formatDesc(validate.getErrorMessages().stream().map(BeanValidationResult.ErrorMessage::getMessage).collect(Collectors.joining(","))));
            }
            gatewayAuthorizeService.add(BeanUtil.toBean(gatewayAuthorizeImport, GatewayAuthorize.class));
        }
        return WebResponse.buildSuccess(AdminReturnCode.SUCCESS_IMPORT.getFormatDesc(importList.size()));
    }

    /**
     * 修改接入方服务授权表
     *
     * @param gatewayAuthorizeImport gatewayAuthorizeImport
     * @return WebResponse
     */
    @SysLog(remark = "修改接入方服务授权表", type = LogType.EDIT)
    @PostMapping("edit")
    @Permission("admin:gateway:authorize:edit")
    public WebResponse edit(@Validated({Edit.class}) GatewayAuthorizeImport gatewayAuthorizeImport) {
        if (gatewayAuthorizeImport.getId() == null) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        gatewayAuthorizeService.edit(BeanUtil.toBean(gatewayAuthorizeImport, GatewayAuthorize.class));
        return WebResponse.buildSuccess();
    }

    /**
     * 根据主键删除接入方服务授权表
     *
     * @param ids ids
     * @return WebResponse
     */
    @SysLog(remark = "删除接入方服务授权表", type = LogType.DELETE)
    @PostMapping("delete")
    @Permission("admin:gateway:authorize:delete")
    public WebResponse delete(Long[] ids) {
        if (ArrayUtils.isEmpty(ids)) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        int num = gatewayAuthorizeService.deleteBatch(ids);
        return WebResponse.buildSuccess(AdminReturnCode.SUCCESS_DELETE.getFormatDesc(num));
    }

    @Permission("admin:gateway:authorize:updateCache")
    @PostMapping("updateCache")
    public WebResponse updateCache(String appId, String service) {
        GatewayCache gatewayCache = new GatewayCache();
        gatewayCache.setAppId(appId);
        gatewayCache.setService(service);
        MqUtil.getProducer().sendAsyncMessage(MqTopicEnum.TOPIC_CACHE_REFRESH,
                CacheTypeEnum.GATEWAY_APP_SERVICE, System.currentTimeMillis() + "", JSON.toJSONString(gatewayCache));
        return WebResponse.buildSuccess();
    }

}
