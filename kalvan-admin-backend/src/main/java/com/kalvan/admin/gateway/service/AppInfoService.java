package com.kalvan.admin.gateway.service;

import com.kalvan.admin.system.service.BaseAuditService;
import com.kalvan.client.constant.SwitchEnum;
import com.kalvan.client.exception.BizException;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.db.DataSourceConstants;
import com.kalvan.commons.db.gateway.mapper.AppInfoMapper;
import com.kalvan.commons.db.gateway.model.AppInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;

/**
 * app信息Service实现类
 *
 * @author kalvan
 */
@Slf4j
@Service("appInfoService")
public class AppInfoService extends BaseAuditService<AppInfo> {
    @Resource
    AppInfoMapper mapper;

    @Override
    public void processParams(Map<String, Object> params) {
        // TODO 查询和下载的条件检查
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int add(AppInfo appInfo) {
        AppInfo appInfoQuery = new AppInfo();
        appInfoQuery.setAppId(appInfo.getAppId());
        if (mapper.selectCount(appInfoQuery) > 0) {
            throw new BizException(AdminReturnCode.DATA_REPEAT);
        }
        appInfo.setState(SwitchEnum.OPEN.code);
        return this.addDataAudit(appInfo);
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int edit(AppInfo appInfo) {
        AppInfo appInfoDb = mapper.selectByUk(appInfo.getId());
        if (appInfoDb == null) {
            throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
        }
        return this.editDataAudit(appInfoDb, appInfo);
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int deleteBatch(Object[] ids) {
        int resultNum = 0;
        for (Object id : ids) {
            AppInfo appInfoDb = mapper.selectByUk(id);
            if (appInfoDb == null) {
                throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
            }
            resultNum = resultNum + this.deleteDataAudit(appInfoDb);
        }
        return resultNum;
    }
}
