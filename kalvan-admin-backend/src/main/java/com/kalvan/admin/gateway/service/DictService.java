package com.kalvan.admin.gateway.service;

import com.kalvan.admin.dict.CacheUtil;
import com.kalvan.admin.system.service.BaseAuditService;
import com.kalvan.client.exception.BizException;
import com.kalvan.client.system.SystemUtil;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.db.DataSourceConstants;
import com.kalvan.commons.db.gateway.mapper.DictMapper;
import com.kalvan.commons.db.gateway.model.Dict;
import com.kalvan.web.feign.client.res.DictResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Map;

/**
 * 数据字典配置Service实现类
 *
 * @author kalvan
 * @date 2020-08-23 15:07:36
 */
@Slf4j
@Service
public class DictService extends BaseAuditService<Dict> {
    @Resource
    DictMapper mapper;

    @Override
    public void processParams(Map<String, Object> params) {
        // TODO 查询和下载的条件检查
    }

    @Override
    @Transactional(value = DataSourceConstants.MANAGER_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int add(Dict dict) {
        // TODO 填充数据,选择字段进行检查，更新人，更新时间，查重
        Dict dictQuery = new Dict();
        dictQuery.setDictType(dict.getDictType());
        dictQuery.setDictKey(dict.getDictKey());
        dictQuery.setDictValue(dict.getDictValue());
        if (mapper.selectCount(dictQuery) > 0) {
            throw new BizException(AdminReturnCode.DATA_REPEAT.formatDesc(dict.getDictKey()));
        }
        Collection<DictResponse> dictList = CacheUtil.cache.getDictMap(dict.getDictType()).values();
        if (!dictList.isEmpty()) {
            dict.setDictName(dictList.stream().findFirst().get().getDictName());
        }
        if (StringUtils.isBlank(dict.getSystemCode())) {
            dict.setSystemCode(SystemUtil.constants.getName());
        }
        return mapper.insertSelective(dict);
    }

    @Override
    @Transactional(value = DataSourceConstants.MANAGER_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int edit(Dict dict) {
        Dict dictDb = mapper.selectByUk(dict.getId());
        if (dictDb == null) {
            throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
        }
        // TODO 更新指定字段，更新人
        Dict dictNew = new Dict();
        dictNew.setId(dict.getId());
        dictNew.setDictType(dict.getDictType());
        dictNew.setDictName(dict.getDictName());
        dictNew.setDictKey(dict.getDictKey());
        dictNew.setDictValue(dict.getDictValue());
        dictNew.setDictIcon(dict.getDictIcon());
        dictNew.setSelectEnable(dict.getSelectEnable());
        dictNew.setPriority(dict.getPriority());
        return mapper.updateByUkSelective(dictNew);
    }

    @Override
    @Transactional(value = DataSourceConstants.MANAGER_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int deleteBatch(Object[] ids) {
        int resultNum = 0;
        for (Object id : ids) {
            resultNum = resultNum + mapper.deleteByUk(id);
        }
        return resultNum;
    }

}
