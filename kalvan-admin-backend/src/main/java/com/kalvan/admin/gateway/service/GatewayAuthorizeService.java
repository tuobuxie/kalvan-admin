package com.kalvan.admin.gateway.service;

import com.kalvan.admin.system.service.BaseAuditService;
import com.kalvan.client.constant.SwitchEnum;
import com.kalvan.client.exception.BizException;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.db.DataSourceConstants;
import com.kalvan.commons.db.gateway.mapper.GatewayAuthorizeMapper;
import com.kalvan.commons.db.gateway.model.GatewayAuthorize;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 接入方服务授权表Service实现类
 *
 * @author kalvan
 */
@Slf4j
@Service("gatewayAuthorizeService")
public class GatewayAuthorizeService extends BaseAuditService<GatewayAuthorize> {
    @Resource
    GatewayAuthorizeMapper mapper;

    @Override
    public void processParams(Map<String, Object> params) {
        // TODO 查询和下载的条件检查
        convertBetweenParams(params, "maxAccess", false, false);
        convertBetweenParams(params, "times", false, false);
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int add(GatewayAuthorize gatewayAuthorize) {
        //TODO 新增数据检查
        GatewayAuthorize gatewayAuthorizeQuery = new GatewayAuthorize();
        if (mapper.selectCount(gatewayAuthorizeQuery) > 0) {
            throw new BizException(AdminReturnCode.DATA_REPEAT);
        }
        gatewayAuthorize.setState(SwitchEnum.OPEN.code);
        return this.addDataAudit(gatewayAuthorize);
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int edit(GatewayAuthorize gatewayAuthorize) {
        //TODO 修改数据检查
        GatewayAuthorize gatewayAuthorizeDb = mapper.selectByUk(gatewayAuthorize.getId());
        if (gatewayAuthorizeDb == null) {
            throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
        }
        return this.editDataAudit(gatewayAuthorizeDb, gatewayAuthorize);
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int deleteBatch(Object[] ids) {
        int resultNum = 0;
        for (Object id : ids) {
            GatewayAuthorize gatewayAuthorizeDb = mapper.selectByUk(id);
            if (gatewayAuthorizeDb == null) {
                throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
            }
            resultNum = resultNum + this.deleteDataAudit(gatewayAuthorizeDb);
        }
        return resultNum;
    }
}
