package com.kalvan.admin.gateway.service;

import com.kalvan.admin.system.service.BaseAuditService;
import com.kalvan.client.constant.SwitchEnum;
import com.kalvan.client.exception.BizException;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.db.DataSourceConstants;
import com.kalvan.commons.db.gateway.mapper.GatewayRouteMapper;
import com.kalvan.commons.db.gateway.model.GatewayRoute;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 网关路由表Service实现类
 *
 * @author kalvan
 */
@Slf4j
@Service("gatewayRouteService")
public class GatewayRouteService extends BaseAuditService<GatewayRoute> {
    @Resource
    GatewayRouteMapper mapper;

    @Override
    public void processParams(Map<String, Object> params) {
        // TODO 查询和下载的条件检查
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int add(GatewayRoute gatewayRoute) {
        //TODO 新增数据检查
        GatewayRoute gatewayRouteQuery = new GatewayRoute();
        gatewayRouteQuery.setId(gatewayRoute.getId());
        if (mapper.selectCount(gatewayRouteQuery) > 0) {
            throw new BizException(AdminReturnCode.DATA_REPEAT);
        }
        gatewayRoute.setState(SwitchEnum.OPEN.code);
        return this.addDataAudit(gatewayRoute);
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int edit(GatewayRoute gatewayRoute) {
        //TODO 修改数据检查
        GatewayRoute gatewayRouteDb = mapper.selectByUk(gatewayRoute.getId());
        if (gatewayRouteDb == null) {
            throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
        }
        return this.editDataAudit(gatewayRouteDb, gatewayRoute);
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int deleteBatch(Object[] ids) {
        int resultNum = 0;
        for (Object id : ids) {
            GatewayRoute gatewayRouteDb = mapper.selectByUk(id);
            if (gatewayRouteDb == null) {
                throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
            }
            resultNum = resultNum + this.deleteDataAudit(gatewayRouteDb);
        }
        return resultNum;
    }
}
