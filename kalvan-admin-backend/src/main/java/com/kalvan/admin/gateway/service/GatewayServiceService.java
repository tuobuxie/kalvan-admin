package com.kalvan.admin.gateway.service;

import com.kalvan.admin.system.service.BaseAuditService;
import com.kalvan.client.constant.SwitchEnum;
import com.kalvan.client.exception.BizException;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.db.DataSourceConstants;
import com.kalvan.commons.db.gateway.mapper.GatewayServiceMapper;
import com.kalvan.commons.db.gateway.model.GatewayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 服务发布Service实现类
 *
 * @author kalvan
 */
@Slf4j
@Service("gatewayServiceService")
public class GatewayServiceService extends BaseAuditService<GatewayService> {
    @Resource
    GatewayServiceMapper mapper;

    @Override
    public void processParams(Map<String, Object> params) {
        // TODO 查询和下载的条件检查
        convertBetweenParams(params, "maxAccess", false, false);
        convertBetweenParams(params, "times", false, false);
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int add(GatewayService gatewayService) {
        //TODO 新增数据检查
        GatewayService gatewayServiceQuery = new GatewayService();
        if (mapper.selectCount(gatewayServiceQuery) > 0) {
            throw new BizException(AdminReturnCode.DATA_REPEAT);
        }
        gatewayService.setState(SwitchEnum.OPEN.code);
        return this.addDataAudit(gatewayService);
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int edit(GatewayService gatewayService) {
        //TODO 修改数据检查
        GatewayService gatewayServiceDb = mapper.selectByUk(gatewayService.getId());
        if (gatewayServiceDb == null) {
            throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
        }
        return this.editDataAudit(gatewayServiceDb, gatewayService);
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int deleteBatch(Object[] ids) {
        int resultNum = 0;
        for (Object id : ids) {
            GatewayService gatewayServiceDb = mapper.selectByUk(id);
            if (gatewayServiceDb == null) {
                throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
            }
            resultNum = resultNum + this.deleteDataAudit(gatewayServiceDb);
        }
        return resultNum;
    }
}
