package com.kalvan.admin.gateway.service;

import com.kalvan.admin.system.service.BaseAuditService;
import com.kalvan.client.constant.SwitchEnum;
import com.kalvan.client.exception.BizException;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.db.DataSourceConstants;
import com.kalvan.commons.db.gateway.mapper.GrayRuleMapper;
import com.kalvan.commons.db.gateway.model.GrayRule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 灰度规则配置Service实现类
 *
 * @author kalvan
 */
@Slf4j
@Service("grayRuleService")
public class GrayRuleService extends BaseAuditService<GrayRule> {
    @Resource
    GrayRuleMapper mapper;

    @Override
    public void processParams(Map<String, Object> params) {
        // TODO 查询和下载的条件检查
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int add(GrayRule grayRule) {
        //TODO 新增数据检查
        GrayRule grayRuleQuery = new GrayRule();
        if (mapper.selectCount(grayRuleQuery) > 0) {
            throw new BizException(AdminReturnCode.DATA_REPEAT);
        }
        grayRule.setState(SwitchEnum.OPEN.code);
        return this.addDataAudit(grayRule);
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int edit(GrayRule grayRule) {
        //TODO 修改数据检查
        GrayRule grayRuleDb = mapper.selectByUk(grayRule.getId());
        if (grayRuleDb == null) {
            throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
        }
        return this.editDataAudit(grayRuleDb, grayRule);
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int deleteBatch(Object[] ids) {
        int resultNum = 0;
        for (Object id : ids) {
            GrayRule grayRuleDb = mapper.selectByUk(id);
            if (grayRuleDb == null) {
                throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
            }
            resultNum = resultNum + this.deleteDataAudit(grayRuleDb);
        }
        return resultNum;
    }
}
