package com.kalvan.admin.gateway.service;

import com.kalvan.admin.system.service.BaseAuditService;
import com.kalvan.client.constant.SwitchEnum;
import com.kalvan.client.exception.BizException;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.db.DataSourceConstants;
import com.kalvan.commons.db.gateway.mapper.GrayServiceMapper;
import com.kalvan.commons.db.gateway.model.GrayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 灰度服务配置Service实现类
 *
 * @author kalvan
 */
@Slf4j
@Service("grayServiceService")
public class GrayServiceService extends BaseAuditService<GrayService> {
    @Resource
    GrayServiceMapper mapper;

    @Override
    public void processParams(Map<String, Object> params) {
        // TODO 查询和下载的条件检查
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int add(GrayService grayService) {
        //TODO 新增数据检查
        GrayService grayServiceQuery = new GrayService();
        if (mapper.selectCount(grayServiceQuery) > 0) {
            throw new BizException(AdminReturnCode.DATA_REPEAT);
        }
        grayService.setState(SwitchEnum.OPEN.code);
        return this.addDataAudit(grayService);
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int edit(GrayService grayService) {
        //TODO 修改数据检查
        GrayService grayServiceDb = mapper.selectByUk(grayService.getId());
        if (grayServiceDb == null) {
            throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
        }
        return this.editDataAudit(grayServiceDb, grayService);
    }

    @Override
    @Transactional(value = DataSourceConstants.GATEWAY_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int deleteBatch(Object[] ids) {
        int resultNum = 0;
        for (Object id : ids) {
            GrayService grayServiceDb = mapper.selectByUk(id);
            if (grayServiceDb == null) {
                throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
            }
            resultNum = resultNum + this.deleteDataAudit(grayServiceDb);
        }
        return resultNum;
    }

}
