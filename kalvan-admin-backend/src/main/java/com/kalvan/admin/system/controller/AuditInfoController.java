package com.kalvan.admin.system.controller;


import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.kalvan.admin.annotation.Permission;
import com.kalvan.admin.annotation.SysLog;
import com.kalvan.admin.excel.ExcelUtil;
import com.kalvan.admin.log.LogType;
import com.kalvan.admin.system.service.AuditInfoService;
import com.kalvan.admin.system.service.BaseAuditService;
import com.kalvan.client.exception.BizException;
import com.kalvan.client.model.WebResponse;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.constant.AuditState;
import com.kalvan.commons.constant.LogGroup;
import com.kalvan.commons.db.manager.mapper.AuditInfoMapper;
import com.kalvan.commons.db.manager.model.AuditInfo;
import com.kalvan.db.mybatis.PageInfo;
import com.kalvan.web.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.ResolvableType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

/**
 * 审核信息Controller
 *
 * @author kalvan.tools
 */
@SysLog(group = LogGroup.AUDIT_INFO)
@RestController
@RequestMapping("system/auditinfo")
@Slf4j
public class AuditInfoController extends BaseController {
    @Resource
    AuditInfoService auditInfoService;

    /**
     * 分页查询
     *
     * @param params 查询参数
     * @return WebResponse
     */
    @PostMapping("list")
    @Permission("admin:system:auditinfo:list")
    public WebResponse list(PageInfo pageInfo, @RequestParam Map<String, Object> params) {
        Page<AuditInfo> data = auditInfoService.queryPage(pageInfo, params);
        return WebResponse.buildSuccess().putPage(data);
    }

    /**
     * 根据主键查询详情
     *
     * @param id 主键
     * @return WebResponse
     */
    @GetMapping("info/{id}")
    @Permission("admin:system:auditinfo:info")
    public WebResponse info(@PathVariable("id") Long id) {
        AuditInfo auditInfo = auditInfoService.findById(id);
        if (auditInfo == null) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        return WebResponse.buildSuccess().putData(auditInfo);
    }

    private static final int DOWNLOAD_MAX_COUNT = 20000;

    /**
     * 下载
     *
     * @param params   查询参数
     * @param response response
     * @throws Exception 抛出异常拦截器统一处理
     */
    @SysLog(remark = "导出审核信息", type = LogType.EXPORT)
    @PostMapping("download")
    @Permission("admin:system:auditinfo:download")
    public void download(@RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        PageInfo pageInfo = new PageInfo();
        pageInfo.setPage(1);
        pageInfo.setLimit(DOWNLOAD_MAX_COUNT);
        Page<AuditInfo> data = auditInfoService.queryPage(pageInfo, params);
        String filename = "审核信息";
        if (data.getTotal() > DOWNLOAD_MAX_COUNT) {
            throw new BizException(AdminReturnCode.PARAM_REGION_OVER.formatDesc("下载条数", DOWNLOAD_MAX_COUNT));
        }
        filename = filename + DatePattern.PURE_DATETIME_MS_FORMAT.format(DateUtil.date());
        ExcelUtil.download(filename, response, AuditInfo.class, data);
    }

    @Permission("admin:system:auditinfo:auditing")
    @PostMapping("auditing")
    public WebResponse auditing(Long[] ids, boolean pass) {
        if (ArrayUtils.isEmpty(ids)) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        int row=auditInfoService.auditing(ids, pass);
        return WebResponse.buildSuccess(AdminReturnCode.SUCCESS_AUDITING.getFormatDesc(row));
    }
}
