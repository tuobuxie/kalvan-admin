package com.kalvan.admin.system.controller;


import cn.hutool.core.bean.BeanUtil;
import com.kalvan.admin.annotation.Permission;
import com.kalvan.admin.system.service.AuthService;
import com.kalvan.admin.system.service.MenuService;
import com.kalvan.admin.valid.Add;
import com.kalvan.admin.valid.Edit;
import com.kalvan.client.exception.BizException;
import com.kalvan.client.model.WebResponse;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.constant.LogGroup;
import com.kalvan.commons.db.manager.model.Menu;
import com.kalvan.commons.dto.system.MenuImport;
import com.kalvan.admin.annotation.SysLog;
import com.kalvan.web.controller.BaseController;
import com.kalvan.web.annotation.I18n;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 菜单资源Controller
 *
 * @author kalvan
 * @date 2019-06-01 02:29:27
 */
@I18n("i18n/com/kalvan/admin/system/menu")
@SysLog(group = LogGroup.MENU)
@RestController
@RequestMapping("system/menu")
@Slf4j
public class MenuController extends BaseController {
    @Resource
    private MenuService menuService;
    @Resource
    private AuthService authService;

    /**
     * 查询所有
     *
     * @return WebResponse
     */
    @PostMapping("list")
    @Permission("admin:system:menu:list")
    public WebResponse list(String systemCode) {
        List<Menu> data = authService.selectMenuList();
        return WebResponse.buildSuccess().putData(data);
    }

    /**
     * 根据主键查询详情
     *
     * @param id 主键
     * @return WebResponse
     */
    @GetMapping("info/{id}")
    @Permission("admin:system:menu:info")
    public WebResponse info(@PathVariable("id") Long id) {
        Menu menu = menuService.findById(id);
        if (menu == null) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        return WebResponse.buildSuccess().putData(menu);
    }

    /**
     * 新增菜单资源
     *
     * @param menuImport menuImport
     * @return WebResponse
     */
    @SysLog(remark = "新增菜单资源")
    @PostMapping("add")
    @Permission("admin:system:menu:add")
    public WebResponse add(@Validated({Add.class}) MenuImport menuImport) {
        menuService.add(BeanUtil.toBean(menuImport, Menu.class));
        return WebResponse.buildSuccess();
    }

    /**
     * 修改菜单资源
     *
     * @param menuImport menuImport
     * @return WebResponse
     */
    @SysLog(remark = "修改菜单资源")
    @PostMapping("edit")
    @Permission("admin:system:menu:edit")
    public WebResponse edit(@Validated({Edit.class}) MenuImport menuImport) {
        if (menuImport.getId() == null) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        menuService.edit(BeanUtil.toBean(menuImport, Menu.class));
        return WebResponse.buildSuccess();
    }

    /**
     * 根据主键删除菜单资源
     *
     * @param ids ids
     * @return WebResponse
     */
    @SysLog(remark = "删除菜单资源")
    @PostMapping("delete")
    @Permission("admin:system:menu:delete")
    public WebResponse delete(Long[] ids) {
        if (ArrayUtils.isEmpty(ids)) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        int num = menuService.deleteBatch(ids);
        return WebResponse.buildSuccess(String.format("成功删除%s行数据", num));
    }

    @PostMapping("priority")
    @Permission("admin:system:menu:edit")
    public WebResponse editOrder(Long id, Integer priority) {
        if (id == null || priority == null) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        Menu menu = new Menu();
        menu.setPriority(priority);
        menu.setId(id);
        menuService.edit(menu);
        return WebResponse.buildSuccess();
    }

}
