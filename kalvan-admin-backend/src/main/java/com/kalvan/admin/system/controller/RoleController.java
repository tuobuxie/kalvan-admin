package com.kalvan.admin.system.controller;


import cn.hutool.core.bean.BeanUtil;
import com.github.pagehelper.Page;
import com.kalvan.admin.annotation.Permission;
import com.kalvan.admin.annotation.SysLog;
import com.kalvan.admin.system.service.AuthService;
import com.kalvan.admin.system.service.RoleService;
import com.kalvan.admin.valid.Add;
import com.kalvan.admin.valid.Edit;
import com.kalvan.client.exception.BizException;
import com.kalvan.client.model.WebResponse;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.constant.LogGroup;
import com.kalvan.commons.db.manager.mapper.MenuMapper;
import com.kalvan.commons.db.manager.mapper.RoleMenuMapper;
import com.kalvan.commons.db.manager.model.Menu;
import com.kalvan.commons.db.manager.model.Role;
import com.kalvan.commons.db.manager.model.RoleMenu;
import com.kalvan.commons.dto.system.RoleImport;
import com.kalvan.db.mybatis.PageInfo;
import com.kalvan.web.annotation.I18n;
import com.kalvan.web.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 角色Controller
 *
 * @author kalvan
 * @date 2019-06-01 02:29:27
 */
@I18n("i18n/com/kalvan/admin/system/role")
@SysLog(group = LogGroup.ROLE)
@RestController
@RequestMapping("system/role")
@Slf4j
public class RoleController extends BaseController {
    @Resource
    private RoleService roleService;
    @Resource
    private AuthService authService;
    @Resource
    private RoleMenuMapper roleMenuMapper;
    @Resource
    private MenuMapper menuMapper;

    /**
     * 分页查询
     *
     * @param params 查询参数
     * @return WebResponse
     */
    @PostMapping("list")
    @Permission("admin:system:role:list")
    public WebResponse list(PageInfo pageInfo, @RequestParam Map<String, Object> params) {
        Page data = roleService.queryPage(pageInfo, params);
        return WebResponse.buildSuccess().putPage(data);
    }

    /**
     * 根据主键查询详情
     *
     * @param id 主键
     * @return WebResponse
     */
    @GetMapping("info/{id}")
    @Permission("admin:system:role:info")
    public WebResponse info(@PathVariable("id") Long id) {
        Role role = roleService.findById(id);
        if (role == null) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        return WebResponse.buildSuccess().putData(role);
    }

    /**
     * 新增角色
     *
     * @param roleImport roleImport
     * @return WebResponse
     */
    @SysLog(remark = "新增角色")
    @PostMapping("add")
    @Permission("admin:system:role:add")
    public WebResponse add(@Validated({Add.class}) RoleImport roleImport) {
        roleService.add(BeanUtil.toBean(roleImport, Role.class));
        return WebResponse.buildSuccess();
    }

    /**
     * 修改角色
     *
     * @param roleImport roleImport
     * @return WebResponse
     */
    @SysLog(remark = "修改角色")
    @PostMapping("edit")
    @Permission("admin:system:role:edit")
    public WebResponse edit(@Validated({Edit.class}) RoleImport roleImport) {
        if (roleImport.getId() == null) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        roleService.edit(BeanUtil.toBean(roleImport, Role.class));
        return WebResponse.buildSuccess();
    }

    /**
     * 根据主键删除角色
     *
     * @param ids ids
     * @return WebResponse
     */
    @SysLog(remark = "删除角色")
    @PostMapping("delete")
    @Permission("admin:system:role:delete")
    public WebResponse delete(Long[] ids) {
        if (ArrayUtils.isEmpty(ids)) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        int num = roleService.deleteBatch(ids);
        return WebResponse.buildSuccess(String.format("成功删除%s行数据", num));
    }

    @PostMapping("menuList")
    @Permission("admin:system:role:grant")
    public WebResponse menuList(Long id) {
        List<Menu> roleMenu = menuMapper.selectAllMenuByRoleId(id);
        List<Menu> menuList = authService.selectMenuList();
        return WebResponse.buildSuccess().putData(roleMenu).put("menuList", menuList);
    }

    @SysLog(remark = "角色授权")
    @PostMapping("grant")
    @Permission("admin:system:role:grant")
    public WebResponse grant(Long roleId, Long[] menuIds) {
        if (roleId == null) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        roleService.grant(roleId, menuIds);
        return WebResponse.buildSuccess();
    }

    @GetMapping("allRole")
    public WebResponse allRole() {
        return WebResponse.buildSuccess().putData(roleService.findAll());
    }

    @SysLog(remark = "角色复制")
    @PostMapping("copy")
    @Permission("admin:system:role:grant")
    public WebResponse copy(Long roleId, Long copyRoleId) {
        if (roleId == null) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        RoleMenu query = new RoleMenu();
        query.setRoleId(copyRoleId);
        List<RoleMenu> list = roleMenuMapper.select(query);
        if (!list.isEmpty()) {
            List<Long> menuIds = list.stream().map(RoleMenu::getMenuId).collect(Collectors.toList());
            Long[] longs = new Long[menuIds.size()];
            int num = roleService.grant(roleId, menuIds.toArray(longs));
        }
        return WebResponse.buildSuccess();
    }
}
