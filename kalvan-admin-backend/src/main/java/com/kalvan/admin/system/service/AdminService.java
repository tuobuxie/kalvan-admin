package com.kalvan.admin.system.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.kalvan.client.constant.SwitchEnum;
import com.kalvan.client.exception.BizException;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.db.DataSourceConstants;
import com.kalvan.commons.db.manager.mapper.AdminMapper;
import com.kalvan.commons.db.manager.mapper.AdminRoleMapper;
import com.kalvan.commons.db.manager.model.Admin;
import com.kalvan.commons.db.manager.model.AdminRole;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Condition;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 管理员Service实现类
 *
 * @author kalvan
 * @date 2019-06-01 02:29:27
 */
@Service
public class AdminService extends BaseAuditService<Admin> {
    @Resource
    protected AdminMapper mapper;
    @Resource
    protected AdminRoleMapper adminRoleMapper;

    @Override
    public void processParams(Map<String, Object> params) {
        //  查询和下载的条件检查
    }

    @Override
    @Transactional(value = DataSourceConstants.MANAGER_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int add(Admin admin) {
        //  填充数据,选择字段进行检查，更新人，更新时间
        saveCheck(admin);
        String salt = RandomStringUtils.randomAlphanumeric(16);
        admin.setSalt(salt);
        admin.setPassword(DigestUtil.md5Hex(admin.getPassword() + salt));
        admin.setIsSystem(SwitchEnum.CLOSE.code);
        admin.setLoginCount(0);
        admin.setLoginErrorCount(0);

        int result = mapper.insertSelective(admin);
        if (result > 0) {
            //增加角色
            Long adminId = mapper.selectByUserCode(admin.getUserCode()).getId();
            if (ArrayUtils.isNotEmpty(admin.getRoleId())) {
                for (Long roleId : admin.getRoleId()) {
                    saveAdminRole(adminId, roleId);
                }
            }
        }
        return result;
    }

    @Override
    @Transactional(value = DataSourceConstants.MANAGER_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int edit(Admin admin) {
        Admin adminDb = mapper.selectByUk(admin.getId());
        if (adminDb == null) {
            throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
        }
        if (adminDb.getIsSystem() == SwitchEnum.OPEN.code) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        saveCheck(admin);
        //  更新指定字段，更新人
        Admin adminNew = new Admin();
        adminNew.setId(admin.getId());
        adminNew.setUserCode(admin.getUserCode());
        adminNew.setUserName(admin.getUserName());
        String password = admin.getPassword();
        if (StringUtils.isNotBlank(password)) {
            String newPassword = DigestUtil.md5Hex(password.toLowerCase() + adminDb.getSalt());
            adminNew.setPassword(newPassword);
        }
        adminNew.setMobile(admin.getMobile());
        adminNew.setEmail(admin.getEmail());
        adminNew.setState(admin.getState());
        adminNew.setUpdateTime(DateUtil.date());
        int result = mapper.updateByUkSelective(adminNew);
        if (result > 0) {
            //增加角色
            if (ArrayUtils.isNotEmpty(admin.getRoleId())) {
                deleteAdminRole(adminDb.getId());
                for (Long roleId : admin.getRoleId()) {
                    saveAdminRole(adminDb.getId(), roleId);
                }
            }
        }
        return result;
    }

    @Transactional(value = DataSourceConstants.MANAGER_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int updatePwd(String userCode, String password) {
        Admin adminDb = mapper.selectByUserCode(userCode);
        if (adminDb == null) {
            throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
        }
        Admin adminNew = new Admin();
        adminNew.setId(adminDb.getId());
        adminNew.setPassword(DigestUtil.md5Hex(password + adminDb.getSalt()));
        return mapper.updateByUkSelective(adminNew);
    }

    @Override
    @Transactional(value = DataSourceConstants.MANAGER_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int deleteBatch(Object[] ids) {
        int resultNum = 0;
        for (Object id : ids) {
            deleteAdminRole((Long) id);
            //不能删除系统管理员
            resultNum = resultNum + mapper.deleteNotSystemAdminById((Long) id);
        }
        return resultNum;
    }

    /**
     * 清空用户角色
     */
    private void deleteAdminRole(Long adminId) {
        AdminRole adminRoleDelete = new AdminRole();
        adminRoleDelete.setAdminId(adminId);
        adminRoleMapper.delete(adminRoleDelete);
    }

    /**
     * 新增用户角色
     */
    private void saveAdminRole(Long adminId, Long roleId) {
        AdminRole adminRoleNew = new AdminRole();
        adminRoleNew.setAdminId(adminId);
        adminRoleNew.setRoleId(roleId);
        adminRoleMapper.insertSelective(adminRoleNew);
    }

    private void saveCheck(Admin admin) {
        Condition condition = new Condition(Admin.class);
        condition.createCriteria()
                .andCondition("user_code=", admin.getUserCode())
                .orCondition("mobile=", admin.getMobile())
                .orCondition("email=", admin.getEmail());
        if (mapper.selectCountByCondition(condition) > 0) {
            throw new BizException(AdminReturnCode.DATA_REPEAT.formatDesc(
                    String.format("用户号%s 手机号%s email%s", admin.getUserCode(), admin.getMobile(), admin.getEmail())));
        }
    }

}
