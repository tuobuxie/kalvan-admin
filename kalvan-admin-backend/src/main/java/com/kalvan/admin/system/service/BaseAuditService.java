package com.kalvan.admin.system.service;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.support.spring.PropertyPreFilters;
import com.kalvan.admin.log.LogType;
import com.kalvan.client.constant.CommonReturnCode;
import com.kalvan.client.context.RequestContextHolder;
import com.kalvan.client.exception.BizException;
import com.kalvan.commons.constant.AdminContextCacheKey;
import com.kalvan.commons.constant.AuditState;
import com.kalvan.commons.db.manager.mapper.AuditInfoMapper;
import com.kalvan.commons.db.manager.model.AuditInfo;
import com.kalvan.db.mybatis.BaseService;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import java.util.Map;

/**
 * Service实现类
 *
 * @author kalvan
 */
@Slf4j
public abstract class BaseAuditService<T> extends BaseService<T> {
    @Resource
    AuditInfoMapper auditInfoMapper;

    /**
     * 新增数据审核
     *
     * @param dataNew 新增数据
     * @return row
     */
    public int addDataAudit(T dataNew) {
        AuditInfo auditInfo = new AuditInfo();
        //model名字=LogGroup.定义 方便共用字典
        String service = dataNew.getClass().getSimpleName();
        service = service.replaceAll("([a-z])([A-Z])", "$1" + "_" + "$2").toLowerCase();
        auditInfo.setDataType(service);
        auditInfo.setDataNew(JSON.toJSONString(dataNew));
        auditInfo.setApplicant(String.valueOf(RequestContextHolder.getContext().get(AdminContextCacheKey.USER_NAME)));
        auditInfo.setAuditType(LogType.ADD.code);
        auditInfo.setAuditState(AuditState.WIAT.code);
        return auditInfoMapper.insertSelective(auditInfo);
    }

    /**
     * 更新数据审核
     *
     * @param dataOld 更新前数据
     * @param dataNew 更新后数据
     * @return row
     */
    public int editDataAudit(T dataOld, T dataNew) {
        PropertyPreFilters filters = new PropertyPreFilters();
        PropertyPreFilters.MySimplePropertyPreFilter excludefilter = filters.addFilter();
        excludefilter.addExcludes("createTime", "updateTime");
        AuditInfo auditInfo = new AuditInfo();
        //model名字=LogGroup.定义 方便共用字典
        String service = dataOld.getClass().getSimpleName();
        service = service.replaceAll("([a-z])([A-Z])", "$1" + "_" + "$2").toLowerCase();
        auditInfo.setDataType(service);
        auditInfo.setDataOld(JSON.toJSONString(dataOld, excludefilter));
        auditInfo.setDataNew(JSON.toJSONString(dataNew));
        auditInfo.setApplicant(String.valueOf(RequestContextHolder.getContext().get(AdminContextCacheKey.USER_NAME)));
        auditInfo.setAuditType(LogType.EDIT.code);
        auditInfo.setAuditState(AuditState.WIAT.code);
        return auditInfoMapper.insertSelective(auditInfo);
    }

    /**
     * 删除数据审核
     *
     * @param dataOld 删除前数据
     * @return row
     */
    public int deleteDataAudit(T dataOld) {
        AuditInfo auditInfo = new AuditInfo();
        //model名字=LogGroup.定义 方便共用字典
        String service = dataOld.getClass().getSimpleName();
        service = service.replaceAll("([a-z])([A-Z])", "$1" + "_" + "$2").toLowerCase();
        auditInfo.setDataType(service);
        auditInfo.setDataOld(JSON.toJSONString(dataOld));
        auditInfo.setApplicant(String.valueOf(RequestContextHolder.getContext().get(AdminContextCacheKey.USER_NAME)));
        auditInfo.setAuditType(LogType.DELETE.code);
        auditInfo.setAuditState(AuditState.WIAT.code);
        return auditInfoMapper.insertSelective(auditInfo);
    }

    /**
     * 审核通过后变更数据
     *
     * @param oldObject 更新前数据,如果为空则表示新增
     * @param newObject 更新后数据,如果为空则表示删除
     * @return int
     */
    public int auditingUpdate(T oldObject, T newObject) {
        int row;
        if (oldObject == null) {
            //新增数据
            row = mapper.insertSelective(newObject);
        } else if (newObject == null) {
            //删除数据
            row = mapper.deleteByUk(oldObject);
        } else {
            //修改数据
            T dbObject = mapper.selectByUk(oldObject);
            if (dbObject == null) {
                throw new BizException(CommonReturnCode.DATA_NOT_EXISTS);
            }
            Map<String, Object> dbMap = BeanUtil.beanToMap(dbObject);
            Map<String, Object> oldMap = BeanUtil.beanToMap(oldObject);
            for (String key : oldMap.keySet()) {
                if (oldMap.get(key) != null && !oldMap.get(key).equals(dbMap.get(key))) {
                    //检查数据库数据发生变化则不允许操作
                    throw new BizException(CommonReturnCode.DATA_STATE_ERROR);
                }
            }
            row = mapper.updateByUkSelective(newObject);
        }
        auditingNotify(oldObject, newObject);
        return row;
    }

    /**
     * 数据更新后回调实现
     * 触发缓存更新，通知等事件
     *
     * @param oldObject 更新前数据,如果为空则表示新增
     * @param newObject 更新后数据,如果为空则表示删除
     */
    public void auditingNotify(T oldObject, T newObject) {

    }
}
