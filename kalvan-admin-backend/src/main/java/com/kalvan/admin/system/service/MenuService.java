package com.kalvan.admin.system.service;

import com.kalvan.client.exception.BizException;
import com.kalvan.client.system.SystemUtil;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.db.DataSourceConstants;
import com.kalvan.commons.db.manager.mapper.MenuMapper;
import com.kalvan.commons.db.manager.mapper.RoleMenuMapper;
import com.kalvan.commons.db.manager.model.Menu;
import com.kalvan.commons.db.manager.model.RoleMenu;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 菜单资源Service实现类
 *
 * @author kalvan
 * @date 2019-06-01 02:29:27
 */
@Service
public class MenuService extends BaseAuditService<Menu> {
    @Resource
    private MenuMapper mapper;
    @Resource
    private RoleMenuMapper roleMenuMapper;

    @Override
    public void processParams(Map<String, Object> params) {
        //  查询和下载的条件检查
    }

    @Override
    public int add(Menu menu) {
        //  填充数据,选择字段进行检查，更新人，更新时间
        Menu menuQuery = new Menu();
        menuQuery.setCode(menu.getCode());
        if (mapper.selectCount(menuQuery) > 0) {
            throw new BizException(AdminReturnCode.DATA_REPEAT.formatDesc(menu.getCode()));
        }
        if (menu.getPriority() == null) {
            menu.setPriority(0);
        }
        // 维护parentIds
        if (menu.getParentId() == null || menu.getParentId() == 0) {
            menu.setParentId(0L);
        } else {
            Menu parentMenu = mapper.selectByUk(menu.getParentId());
            menu.setSystemCode(parentMenu.getSystemCode());
        }
        if (StringUtils.isBlank(menu.getSystemCode())) {
            menu.setSystemCode(SystemUtil.constants.getName());
        }
        return mapper.insertSelective(menu);
    }

    @Override
    public int edit(Menu menu) {
        Menu menuDb = mapper.selectByUk(menu.getId());
        if (menuDb == null) {
            throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
        }
        //  更新指定字段，更新人
        Menu menuNew = new Menu();
        menuNew.setId(menu.getId());
        menuNew.setSystemCode(menu.getSystemCode());
        menuNew.setTitle(menu.getTitle());
        menuNew.setName(menu.getName());
        menuNew.setType(menu.getType());
        menuNew.setHideChildren(menu.getHideChildren());
        menuNew.setIcon(menu.getIcon());
        menuNew.setPath(menu.getPath());
        menuNew.setRedirect(menu.getRedirect());
        menuNew.setCode(menu.getCode());
        menuNew.setPriority(menu.getPriority());
        menuNew.setParentId(menu.getParentId());
        return mapper.updateByUkSelective(menuNew);
    }

    @Override
    @Transactional(value = DataSourceConstants.MANAGER_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int deleteBatch(Object[] ids) {
        int resultNum = 0;
        for (Object id : ids) {
            //删除角色菜单
            RoleMenu roleMenuDelete = new RoleMenu();
            roleMenuDelete.setMenuId((Long) id);
            roleMenuMapper.delete(roleMenuDelete);
            resultNum = resultNum + mapper.deleteByUk(id);
        }
        //TODO 删除子菜单
        return resultNum;
    }
}
