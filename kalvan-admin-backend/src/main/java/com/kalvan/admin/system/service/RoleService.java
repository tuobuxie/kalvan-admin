package com.kalvan.admin.system.service;

import com.kalvan.client.exception.BizException;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.db.DataSourceConstants;
import com.kalvan.commons.db.manager.mapper.AdminRoleMapper;
import com.kalvan.commons.db.manager.mapper.RoleMapper;
import com.kalvan.commons.db.manager.mapper.RoleMenuMapper;
import com.kalvan.commons.db.manager.model.AdminRole;
import com.kalvan.commons.db.manager.model.Role;
import com.kalvan.commons.db.manager.model.RoleMenu;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 角色Service实现类
 *
 * @author kalvan
 * @date 2019-06-01 02:29:27
 */
@Service
public class RoleService extends BaseAuditService<Role> {
    @Resource
    protected RoleMapper mapper;
    @Resource
    AdminRoleMapper adminRoleMapper;
    @Resource
    RoleMenuMapper roleMenuMapper;

    @Override
    public void processParams(Map<String, Object> params) {
        //  查询和下载的条件检查
    }

    @Override
    @Transactional(value = DataSourceConstants.MANAGER_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int add(Role role) {
        //  填充数据,选择字段进行检查，更新人，更新时间
        Role roleQuery = new Role();
        roleQuery.setRoleName(role.getRoleName());
        if (mapper.selectCount(roleQuery) > 0) {
            throw new BizException(AdminReturnCode.DATA_REPEAT.formatDesc(role.getRoleName()));
        }
        return mapper.insertSelective(role);
    }

    @Override
    @Transactional(value = DataSourceConstants.MANAGER_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int edit(Role role) {
        Role roleDb = mapper.selectByUk(role.getId());
        if (roleDb == null) {
            throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
        }
        //  更新指定字段，更新人
        Role roleNew = new Role();
        roleNew.setId(role.getId());
        roleNew.setRoleName(role.getRoleName());
        roleNew.setRoleDesc(role.getRoleDesc());
        roleNew.setState(role.getState());
        return mapper.updateByUkSelective(roleNew);
    }

    @Override
    @Transactional(value = DataSourceConstants.MANAGER_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int deleteBatch(Object[] ids) {
        int resultNum = 0;
        for (Object id : ids) {
            //清理拥有该角色的管理员
            AdminRole adminRoleDelete = new AdminRole();
            adminRoleDelete.setRoleId((Long) id);
            adminRoleMapper.delete(adminRoleDelete);

            //清理角色对应菜单
            RoleMenu roleMenuDelete = new RoleMenu();
            roleMenuDelete.setRoleId((Long) id);
            roleMenuMapper.delete(roleMenuDelete);
            resultNum = resultNum + mapper.deleteByUk(id);
        }
        return resultNum;
    }

    @Transactional(value = DataSourceConstants.MANAGER_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int grant(Long roleId, Long[] menuIds) {
        int resultNum = 0;
        //  应该要备份原有信息
        RoleMenu roleMenuDelete = new RoleMenu();
        roleMenuDelete.setRoleId(roleId);
        roleMenuMapper.delete(roleMenuDelete);
        if (ArrayUtils.isNotEmpty(menuIds)) {
            for (Long menuId : menuIds) {
                RoleMenu roleMenu = new RoleMenu();
                roleMenu.setMenuId(menuId);
                roleMenu.setRoleId(roleId);
                try {
                    resultNum = resultNum + roleMenuMapper.insertSelective(roleMenu);
                } catch (Exception e) {
                    // 重复提交重复跳过
                }
            }
        }
        return resultNum;
    }

}
