package com.kalvan.admin.trade.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.extra.validation.BeanValidationResult;
import cn.hutool.extra.validation.ValidationUtil;
import com.github.pagehelper.Page;
import com.kalvan.admin.annotation.ParamsDecrypted;
import com.kalvan.admin.annotation.Permission;
import com.kalvan.admin.annotation.SysLog;
import com.kalvan.admin.excel.ExcelUtil;
import com.kalvan.admin.log.LogType;
import com.kalvan.admin.valid.Add;
import com.kalvan.admin.valid.Edit;
import com.kalvan.client.constant.CommonReturnCode;
import com.kalvan.client.exception.BizException;
import com.kalvan.client.model.WebResponse;
import com.kalvan.db.mybatis.PageInfo;
import com.kalvan.web.controller.BaseController;
import com.kalvan.admin.trade.service.DemoService;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.constant.LogGroup;
import com.kalvan.commons.db.trade.model.Demo;
import com.kalvan.commons.dto.trade.DemoImport;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 测试流水表Controller
 *
 * @author kalvan
 */
@SysLog(group = LogGroup.DEMO)
@RestController
@RequestMapping("trade/demo")
@Slf4j
public class DemoController extends BaseController {
    @Resource
    DemoService demoService;

    /**
     * 分页查询
     *
     * @param params 查询参数
     * @return WebResponse
     */
    @PostMapping("list")
    @Permission("admin:trade:demo:list")
    public WebResponse list(PageInfo pageInfo, @RequestParam Map<String, Object> params) {
        Page<Demo> data = demoService.queryPage(pageInfo, params);
        return WebResponse.buildSuccess().putPage(data);
    }

    /**
     * 汇总查询
     *
     * @param params 查询参数
     * @return WebResponse
     */
    @PostMapping("sum")
    @Permission("admin:trade:demo:sum")
    public WebResponse sum(@RequestParam Map<String, Object> params) {
        Map<String, String> data = demoService.querySum(params);
        return WebResponse.buildSuccess().putData(data);
    }

    /**
     * 根据主键查询详情
     *
     * @param id 主键
     * @return WebResponse
     */
    @GetMapping("info/{id}")
    @Permission("admin:trade:demo:info")
    public WebResponse info(@PathVariable("id") String id) {
        Demo demo = demoService.findById(id);
        if (demo == null) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        return WebResponse.buildSuccess().putData(demo);
    }

    private static final int DOWNLOAD_MAX_COUNT = 20000;

    /**
     * 下载
     *
     * @param params   查询参数
     * @param response response
     * @throws Exception 抛出异常拦截器统一处理
     */
    @SysLog(remark = "导出测试流水表", type = LogType.EXPORT)
    @PostMapping("download")
    @Permission("admin:trade:demo:download")
    public void download(@RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        PageInfo pageInfo = new PageInfo();
        pageInfo.setPage(1);
        pageInfo.setLimit(DOWNLOAD_MAX_COUNT);
        Page<Demo> data = demoService.queryPage(pageInfo, params);
        String filename = "测试流水表";
        if (data.getTotal() > DOWNLOAD_MAX_COUNT) {
            throw new BizException(AdminReturnCode.PARAM_REGION_OVER.formatDesc("下载条数", DOWNLOAD_MAX_COUNT));
        }
        filename = filename + DatePattern.PURE_DATETIME_MS_FORMAT.format(DateUtil.date());
        Set<String> excludeColumnFiledNames = new HashSet<>();
        excludeColumnFiledNames.add("id");
        ExcelUtil.download(filename, response, Demo.class, data, excludeColumnFiledNames);
    }

    /**
     * 新增测试流水表
     *
     * @param demoImport demoImport
     * @return WebResponse
     */
    @SysLog(remark = "新增测试流水表", type = LogType.ADD)
    @PostMapping("add")
    @Permission("admin:trade:demo:add")
    public WebResponse add(@Validated({Add.class}) DemoImport demoImport) {
        demoService.add(BeanUtil.toBean(demoImport, Demo.class));
        return WebResponse.buildSuccess("待审核");
    }

    /**
     * 测试流水表导入模板下载
     *
     * @param response response
     * @throws Exception 抛出异常拦截器统一处理
     */
    @GetMapping("downloadTemplate")
    @Permission("admin:trade:demo:upload")
    public void downloadTemplate(HttpServletResponse response) throws Exception {
        ExcelUtil.downloadTemplate(templateRootPath + "com/kalvan/admin/测试流水表-模板.xls", response);
    }

    /**
     * 导入测试流水表
     *
     * @param file file
     * @return WebResponse
     */
    @SysLog(remark = "导入测试流水表", type = LogType.IMPORT)
    @PostMapping("upload")
    @Permission("admin:trade:demo:upload")
    @ParamsDecrypted(required = false)
    public WebResponse upload(@RequestParam MultipartFile file) throws Exception {
        List<DemoImport> importList = ExcelUtil.importExcel(file, 1, 1, DemoImport.class);
        for (DemoImport demoImport : importList) {
            BeanValidationResult validate = ValidationUtil.warpValidate(demoImport, Add.class);
            if (!validate.isSuccess()) {
                throw new BizException(CommonReturnCode.PARAM_ILLEGAL.formatDesc(validate.getErrorMessages().stream().map(BeanValidationResult.ErrorMessage::getMessage).collect(Collectors.joining(","))));
            }
            demoService.add(BeanUtil.toBean(demoImport, Demo.class));
        }
        return WebResponse.buildSuccess(AdminReturnCode.SUCCESS_IMPORT.getFormatDesc(importList.size()) + ",待审核");
    }

    /**
     * 修改测试流水表
     *
     * @param demoImport demoImport
     * @return WebResponse
     */
    @SysLog(remark = "修改测试流水表", type = LogType.EDIT)
    @PostMapping("edit")
    @Permission("admin:trade:demo:edit")
    public WebResponse edit(@Validated({Edit.class}) DemoImport demoImport) {
        if (StringUtils.isBlank(demoImport.getTransId())) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        int num = demoService.edit(BeanUtil.toBean(demoImport, Demo.class));
        return WebResponse.buildSuccess(AdminReturnCode.SUCCESS_UPDATE.getFormatDesc(num) + ",待审核");
    }

    /**
     * 根据主键删除测试流水表
     *
     * @param ids ids
     * @return WebResponse
     */
    @SysLog(remark = "删除测试流水表", type = LogType.DELETE)
    @PostMapping("delete")
    @Permission("admin:trade:demo:delete")
    public WebResponse delete(String[] ids) {
        if (ArrayUtils.isEmpty(ids)) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        int num = demoService.deleteBatch(ids);
        return WebResponse.buildSuccess(AdminReturnCode.SUCCESS_DELETE.getFormatDesc(num) + ",待审核");
    }

    @Permission("admin:trade:demo:test")
    @PostMapping("test")
    public WebResponse test(String[] ids) {
        //TODO 业务检查和处理
        if (ArrayUtils.isEmpty(ids)) {
            throw new BizException(AdminReturnCode.REQUEST_ILLEGAL);
        }
        return WebResponse.buildSuccess();
    }

}
