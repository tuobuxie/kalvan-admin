package com.kalvan.admin.trade.service;

import com.kalvan.admin.system.service.BaseAuditService;
import com.kalvan.client.constant.SwitchEnum;
import com.kalvan.client.exception.BizException;
import com.kalvan.commons.constant.AdminReturnCode;
import com.kalvan.commons.db.DataSourceConstants;
import com.kalvan.commons.db.trade.mapper.DemoMapper;
import com.kalvan.commons.db.trade.model.Demo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 测试流水表Service实现类
 *
 * @author kalvan
 */
@Slf4j
@Service("demoService")
public class DemoService extends BaseAuditService<Demo> {
    @Resource
    DemoMapper mapper;

    @Override
    public void processParams(Map<String, Object> params) {
        // TODO 查询和下载的条件检查
        convertBetweenParams(params, "orderDate", false, false);
        convertBetweenParams(params, "orderAmount", false, false);
        convertBetweenParams(params, "createTime", true, false);
    }

    @Override
    @Transactional(value = DataSourceConstants.TRADE_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int add(Demo demo) {
        // TODO 填充数据,选择字段进行检查，更新人，更新时间，查重
        if (mapper.selectByUk(demo.getTransId())!=null) {
            throw new BizException(AdminReturnCode.DATA_REPEAT);
        }
        demo.setState(SwitchEnum.OPEN.code);
        return this.addDataAudit(demo);
    }

    @Override
    @Transactional(value = DataSourceConstants.TRADE_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int edit(Demo demo) {
        Demo demoDb = mapper.selectByUk(demo.getTransId());
        if (demoDb == null) {
            throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
        }
        // TODO 最小化精准更新,如果全部字段可以更新则直接使用demo
        Demo demoNew = new Demo();
        demoNew.setId(demo.getId());
        demoNew.setTransId(demo.getTransId());
        demoNew.setTransType(demo.getTransType());
        demoNew.setMerchantNo(demo.getMerchantNo());
        demoNew.setOrderId(demo.getOrderId());
        demoNew.setOrderDate(demo.getOrderDate());
        demoNew.setOrderNote(demo.getOrderNote());
        demoNew.setOrderAmount(demo.getOrderAmount());
        demoNew.setOrderFee(demo.getOrderFee());
        demoNew.setOrderFeeRate(demo.getOrderFeeRate());
        demoNew.setNotifyUrl(demo.getNotifyUrl());
        demoNew.setBankAccountNo(demo.getBankAccountNo());
        demoNew.setBankAccountName(demo.getBankAccountName());
        demoNew.setMobile(demo.getMobile());
        demoNew.setCertificateNo(demo.getCertificateNo());
        demoNew.setState(demo.getState());
        return this.editDataAudit(demoDb, demoNew);
    }

    @Override
    @Transactional(value = DataSourceConstants.TRADE_TRANSACTION_MANAGER, rollbackFor = Exception.class)
    public int deleteBatch(Object[] ids) {
        int resultNum = 0;
        for (Object id : ids) {
            Demo demoDb = mapper.selectByUk(id);
            if (demoDb == null) {
                throw new BizException(AdminReturnCode.DATA_NOT_EXISTS);
            }
            resultNum = resultNum + this.deleteDataAudit(demoDb);
        }
        return resultNum;
    }
}
