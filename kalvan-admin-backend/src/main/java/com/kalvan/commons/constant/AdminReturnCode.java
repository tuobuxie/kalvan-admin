package com.kalvan.commons.constant;

import com.kalvan.client.constant.CommonReturnCode;

/**
 * web系统错误代码定义
 *
 * @author chenliang
 */
public interface AdminReturnCode extends CommonReturnCode {

}
