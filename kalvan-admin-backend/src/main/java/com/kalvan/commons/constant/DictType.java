package com.kalvan.commons.constant;

import com.kalvan.admin.dict.SystemDictType;

/**
 * 数据字典类型定义(扩展)
 *
 * @author kalvan.tools
 */
public interface DictType extends SystemDictType {
    /**
     * app标签字典类型
     */
    String GROUP_NAME = "group_name";
    /**
     * 交易类型字典类型
     */
    String DEMO_TYPE = "demo_type";
    /**
     * 交易状态字典类型
     */
    String DEMO_STATE = "demo_state";

}
