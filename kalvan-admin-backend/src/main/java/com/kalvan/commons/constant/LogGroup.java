package com.kalvan.commons.constant;

/**
 * 操作日志组定义(扩展)
 *
 * @author kalvan.tools
 */
public interface LogGroup {
    /**
     * 鉴权认证
     */
    String AUTH = "auth" ;
    /**
     * 角色日志组<br>
     */
    String ROLE = "role" ;
    /**
     * 管理员日志组<br>
     */
    String ADMIN = "admin" ;
    /**
     * 菜单日志组<br>
     */
    String MENU = "menu" ;
    /**
     * 日志组<br>
     */
    String LOG = "log" ;
    /**
     * 数据字典配置日志组<br>
     */
    String DICT = "dict";
    /**
     * 测试流水表日志组<br>
     */
    String DEMO = "demo";
    /**
     * 接入方信息日志组<br>
     */
    String APP_INFO = "app_info";
    /**
     * 服务发布日志组<br>
     */
    String GATEWAY_SERVICE = "gateway_service";
    /**
     * 接入方服务授权表日志组<br>
     */
    String GATEWAY_AUTHORIZE = "gateway_authorize";
    /**
     * 网关路由表日志组<br>
     */
    String GATEWAY_ROUTE = "gateway_route";
    /**
     * 网关统计日志组<br>
     */
    String GATEWAY_METRIC = "gateway_metric";
    /**
     * 灰度规则配置日志组<br>
     */
    String GRAY_RULE = "gray_rule";
    /**
     * 灰度服务配置日志组<br>
     */
    String GRAY_SERVICE = "gray_service";
    /**
     * 审核信息日志组<br>
     */
    String AUDIT_INFO = "audit_info";
}
