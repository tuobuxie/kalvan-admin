package com.kalvan.commons.db;


/**
 * DataSource配置常量
 *
 * @author kalvan.tools:chenliang
 */
public class DataSourceConstants {
    public static final String MANAGER_MAPPER_PACKAGE = "com.kalvan.commons.db.manager.mapper";
    public static final String MANAGER_MAPPER_PATH = "classpath:/db/manager/mapper/*.xml";
    public static final String MANAGER_SHARDING_YML = "${sharding.jdbc.manager}";
    public static final String MANAGER_DATA_SOURCE = "managerDataSource";
    public static final String MANAGER_TRANSACTION_MANAGER = "managerTransactionManager";
    public static final String MANAGER_SESSION_FACTORY = "managerSessionFactory";

    public static final String TRADE_MAPPER_PACKAGE = "com.kalvan.commons.db.trade.mapper";
    public static final String TRADE_MAPPER_PATH = "classpath:/db/trade/mapper/*.xml";
    public static final String TRADE_SHARDING_YML = "${sharding.jdbc.trade}";
    public static final String TRADE_DATA_SOURCE = "tradeDataSource";
    public static final String TRADE_TRANSACTION_MANAGER = "tradeTransactionManager";
    public static final String TRADE_SESSION_FACTORY = "tradeSessionFactory";

    public static final String GATEWAY_MAPPER_PACKAGE = "com.kalvan.commons.db.gateway.mapper";
    public static final String GATEWAY_MAPPER_PATH = "classpath:/db/gateway/mapper/*.xml";
    public static final String GATEWAY_SHARDING_YML = "${sharding.jdbc.gateway}";
    public static final String GATEWAY_DATA_SOURCE = "gatewayDataSource";
    public static final String GATEWAY_TRANSACTION_MANAGER = "gatewayTransactionManager";
    public static final String GATEWAY_SESSION_FACTORY = "gatewaySessionFactory";

}