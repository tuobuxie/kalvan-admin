package com.kalvan.commons.db.gateway.mapper;

import com.kalvan.commons.db.gateway.model.AppInfo;
import com.kalvan.db.mybatis.IBaseMapper;
import org.springframework.stereotype.Repository;

/**
 * app信息Mapper
 *
 * @author kalvan.tools:kalvan
 */
@Repository
public interface AppInfoMapper extends IBaseMapper<AppInfo> {

}
