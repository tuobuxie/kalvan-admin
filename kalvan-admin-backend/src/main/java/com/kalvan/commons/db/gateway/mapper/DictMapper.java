package com.kalvan.commons.db.gateway.mapper;

import com.kalvan.commons.db.gateway.model.Dict;
import com.kalvan.db.mybatis.IBaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 数据字典配置Mapper
 *
 * @author kalvan.tools:kalvan
 * @date 2020-08-23 15:07:36
 */
@Repository
public interface DictMapper extends IBaseMapper<Dict> {

    /**
     * 查找所有字典类型
     * @return
     */
    List<Dict> selectAllType();
}
