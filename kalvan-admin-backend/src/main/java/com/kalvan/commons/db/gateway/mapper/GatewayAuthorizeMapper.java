package com.kalvan.commons.db.gateway.mapper;

import com.kalvan.commons.db.gateway.model.GatewayAuthorize;
import com.kalvan.db.mybatis.IBaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 接入方服务授权表Mapper
 *
 * @author kalvan.tools:kalvan
 */
@Repository
public interface GatewayAuthorizeMapper extends IBaseMapper<GatewayAuthorize> {

}
