package com.kalvan.commons.db.gateway.mapper;

import com.kalvan.commons.db.gateway.model.GatewayMetric;
import com.kalvan.db.mybatis.IBaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 网关统计Mapper
 *
 * @author kalvan.tools:kalvan
 */
@Repository
public interface GatewayMetricMapper extends IBaseMapper<GatewayMetric> {

}
