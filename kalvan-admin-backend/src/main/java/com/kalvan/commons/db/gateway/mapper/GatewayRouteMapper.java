package com.kalvan.commons.db.gateway.mapper;

import com.kalvan.commons.db.gateway.model.GatewayRoute;
import com.kalvan.db.mybatis.IBaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 网关路由表Mapper
 *
 * @author kalvan.tools:kalvan
 */
@Repository
public interface GatewayRouteMapper extends IBaseMapper<GatewayRoute> {

}
