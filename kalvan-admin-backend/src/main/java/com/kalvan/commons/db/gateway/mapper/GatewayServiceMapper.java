package com.kalvan.commons.db.gateway.mapper;

import com.kalvan.commons.db.gateway.model.GatewayService;
import com.kalvan.db.mybatis.IBaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 服务发布Mapper
 *
 * @author kalvan.tools:kalvan
 */
@Repository
public interface GatewayServiceMapper extends IBaseMapper<GatewayService> {

}
