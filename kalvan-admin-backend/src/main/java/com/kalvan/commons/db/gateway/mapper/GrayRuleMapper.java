package com.kalvan.commons.db.gateway.mapper;

import com.kalvan.commons.db.gateway.model.GrayRule;
import com.kalvan.db.mybatis.IBaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 灰度规则配置Mapper
 *
 * @author kalvan.tools:kalvan
 */
@Repository
public interface GrayRuleMapper extends IBaseMapper<GrayRule> {

}
