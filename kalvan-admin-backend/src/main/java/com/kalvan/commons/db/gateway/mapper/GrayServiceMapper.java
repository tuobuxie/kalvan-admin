package com.kalvan.commons.db.gateway.mapper;

import com.kalvan.commons.db.gateway.model.GrayService;
import com.kalvan.db.mybatis.IBaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 灰度服务配置Mapper
 *
 * @author kalvan.tools:kalvan
 */
@Repository
public interface GrayServiceMapper extends IBaseMapper<GrayService> {

}
