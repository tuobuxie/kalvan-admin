package com.kalvan.commons.db.manager.mapper;

import com.kalvan.commons.db.manager.model.AdminRole;
import com.kalvan.db.mybatis.IBaseMapper;
import org.springframework.stereotype.Service;

/**
 * @author chenliang
 *
 */
@Service
public interface AdminRoleMapper extends IBaseMapper<AdminRole> {
}
