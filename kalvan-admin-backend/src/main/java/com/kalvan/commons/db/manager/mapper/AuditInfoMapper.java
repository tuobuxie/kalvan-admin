package com.kalvan.commons.db.manager.mapper;

import com.kalvan.db.mybatis.IBaseMapper;
import com.kalvan.commons.db.manager.model.AuditInfo;
import org.springframework.stereotype.Repository;

/**
 * 审核信息Mapper
 *
 * @author kalvan.tools:kalvan.tools
 */
@Repository
public interface AuditInfoMapper extends IBaseMapper<AuditInfo> {

}
