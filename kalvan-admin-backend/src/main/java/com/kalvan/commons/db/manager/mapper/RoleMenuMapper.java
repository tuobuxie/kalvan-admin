package com.kalvan.commons.db.manager.mapper;

import com.kalvan.commons.db.manager.model.RoleMenu;
import com.kalvan.db.mybatis.IBaseMapper;
import org.springframework.stereotype.Service;

/**
 * @author chenliang
 *
 */
@Service
public interface RoleMenuMapper extends IBaseMapper<RoleMenu> {
}
