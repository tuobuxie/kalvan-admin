package com.kalvan.commons.db.trade.mapper;

import com.kalvan.db.mybatis.IBaseMapper;
import com.kalvan.commons.db.trade.model.Demo;
import org.springframework.stereotype.Repository;

/**
 * 测试流水表Mapper
 *
 * @author kalvan.tools:kalvan
 */
@Repository
public interface DemoMapper extends IBaseMapper<Demo> {

}
