package com.kalvan.commons.db.trade.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.converters.integer.IntegerStringConverter;
import com.alibaba.excel.converters.longconverter.LongStringConverter;
import com.kalvan.commons.constant.DictType;
import com.kalvan.admin.annotation.Dict;
import com.kalvan.admin.annotation.ExtSuffix;
import com.kalvan.admin.annotation.NumberConvert;
import com.kalvan.db.mybatis.annotation.ShardingTableKey;
import com.kalvan.db.mybatis.annotation.ShardingUk;
import com.kalvan.sensitive.annotation.Desensitized;
import com.kalvan.sensitive.enums.SensitiveType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 测试流水表实体
 * 表名 demo
 *
 * @author kalvan.tools:kalvan
 */
@Getter
@Setter
@Table(name = "demo")
public class Demo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @ExcelProperty(value = "自增id",converter = LongStringConverter.class)
    @Id
    @Column(name = "id")
    private Long id;

    /**
     * 交易流水号
     */
    @ShardingUk
    @ExcelProperty(value = "交易流水号")
    @Column(name = "trans_id")
    private String transId;

    /**
     * 交易类型
     */
    @ExcelProperty(value = "交易类型",converter = IntegerStringConverter.class)
    @Dict(DictType.DEMO_TYPE)
    @Column(name = "trans_type")
    private Integer transType;

    /**
     * 商户号
     */
    @ExcelProperty(value = "商户号",converter = LongStringConverter.class)
    @Column(name = "merchant_no")
    private Long merchantNo;

    /**
     * 订单号
     */
    @ExcelProperty(value = "订单号")
    @Column(name = "order_id")
    private String orderId;

    /**
     * 订单日期
     */
    @ShardingTableKey
    @ExcelProperty(value = "订单日期",converter = IntegerStringConverter.class)
    @Column(name = "order_date")
    private Integer orderDate;

    /**
     * 订单说明
     */
    @ExcelProperty(value = "订单说明")
    @Column(name = "order_note")
    private String orderNote;

    /**
     * 订单金额
     */
    @ExcelProperty(value = "订单金额",converter = LongStringConverter.class)
    @NumberConvert(value = 100)
    @Column(name = "order_amount")
    private Long orderAmount;

    /**
     * 手续费
     */
    @ExcelProperty(value = "手续费",converter = LongStringConverter.class)
    @NumberConvert(value = 100)
    @Column(name = "order_fee")
    private Long orderFee;

    /**
     * 费率
     */
    @ExcelProperty(value = "费率",converter = LongStringConverter.class)
    @ExtSuffix("%")
    @Column(name = "order_fee_rate")
    private Long orderFeeRate;

    /**
     * 通知地址
     */
    @ExcelProperty(value = "通知地址")
    @Column(name = "notify_url")
    private String notifyUrl;

    /**
     * 银行帐号
     */
    @Desensitized(type = SensitiveType.BANK_CARD)
    @ExcelProperty(value = "银行帐号")
    @Column(name = "bank_account_no")
    private String bankAccountNo;

    /**
     * 银行户名
     */
    @Desensitized(type = SensitiveType.CHINESE_NAME)
    @ExcelProperty(value = "银行户名")
    @Column(name = "bank_account_name")
    private String bankAccountName;

    /**
     * 客户手机
     */
    @Desensitized(type = SensitiveType.MOBILE_PHONE)
    @ExcelProperty(value = "客户手机")
    @Column(name = "mobile")
    private String mobile;

    /**
     * 证件号码
     */
    @Desensitized(type = SensitiveType.ID_CARD)
    @ExcelProperty(value = "证件号码")
    @Column(name = "certificate_no")
    private String certificateNo;

    /**
     * 交易状态
     */
    @ExcelProperty(value = "交易状态",converter = IntegerStringConverter.class)
    @Dict(DictType.DEMO_STATE)
    @Column(name = "state")
    private Integer state;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间")
    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @ExcelProperty(value = "更新时间")
    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    @Column(name = "update_time")
    private Date updateTime;

}
