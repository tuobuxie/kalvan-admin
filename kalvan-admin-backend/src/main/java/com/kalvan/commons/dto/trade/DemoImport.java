package com.kalvan.commons.dto.trade;

import com.alibaba.excel.annotation.ExcelProperty;
import com.kalvan.admin.valid.Add;
import lombok.Getter;
import lombok.Setter;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 测试流水表Import 新增修改数据对象支持excel导入
 * 表名 demo
 *
 * @author kalvan.tools:kalvan
 */
@Getter
@Setter
public class DemoImport implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 交易流水号
     */
    @ExcelProperty(index = 0)
    @NotBlank(groups = Add.class, message = "交易流水号不能为空" )
    private String transId;
    /**
     * 交易类型
     */
    @ExcelProperty(index = 1)
    @NotNull(groups = Add.class, message = "交易类型不能为空" )
    private Integer transType;
    /**
     * 商户号
     */
    @ExcelProperty(index = 2)
    @NotNull(groups = Add.class, message = "商户号不能为空" )
    private Long merchantNo;
    /**
     * 订单号
     */
    @ExcelProperty(index = 3)
    @NotBlank(groups = Add.class, message = "订单号不能为空" )
    private String orderId;
    /**
     * 订单日期
     */
    @ExcelProperty(index = 4)
    @NotNull(groups = Add.class, message = "订单日期不能为空" )
    private Integer orderDate;
    /**
     * 订单说明
     */
    @ExcelProperty(index = 5)
    @NotBlank(groups = Add.class, message = "订单说明不能为空" )
    private String orderNote;
    /**
     * 订单金额
     */
    @ExcelProperty(index = 6)
    @NotNull(groups = Add.class, message = "订单金额不能为空" )
    private Long orderAmount;
    /**
     * 手续费
     */
    @ExcelProperty(index = 7)
    @NotNull(groups = Add.class, message = "手续费不能为空" )
    private Long orderFee;
    /**
     * 费率
     */
    @ExcelProperty(index = 8)
    @NotNull(groups = Add.class, message = "费率不能为空" )
    private Long orderFeeRate;
    /**
     * 通知地址
     */
    @ExcelProperty(index = 9)
    @NotBlank(groups = Add.class, message = "通知地址不能为空" )
    private String notifyUrl;
    /**
     * 银行帐号
     */
    @ExcelProperty(index = 10)
    @NotBlank(groups = Add.class, message = "银行帐号不能为空" )
    private String bankAccountNo;
    /**
     * 银行户名
     */
    @ExcelProperty(index = 11)
    @NotBlank(groups = Add.class, message = "银行户名不能为空" )
    private String bankAccountName;
    /**
     * 客户手机
     */
    @ExcelProperty(index = 12)
    @NotBlank(groups = Add.class, message = "客户手机不能为空" )
    private String mobile;
    /**
     * 证件号码
     */
    @ExcelProperty(index = 13)
    @NotBlank(groups = Add.class, message = "证件号码不能为空" )
    private String certificateNo;
    /**
     * 交易状态
     */
    @ExcelProperty(index = 14)
    @NotNull(groups = Add.class, message = "交易状态不能为空" )
    private Integer state;

    /**
     * 主键
     */
    private Long id;
}
