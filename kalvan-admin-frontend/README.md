* [适应场景](#适应场景)
* [快速使用](#快速使用)
* [前端开发](#前端开发)
  * [目录结构](#目录结构)
  * [已集成功能](#已集成功能)
  * [插件生成代码范围](#插件生成代码范围)
* [其他说明](#其他说明)
  * [多页面展示](#多页面展示)
  * [mock](#mock)
  * [登录](#登录)
  * [路由](#路由)
 
# 适应场景
>适用于需要登录、管理界面、权限控制的web项目
>
>可构建管理系统、商户系统、代理商等后台系统
  
# 快速使用
1. 下载后端工程[kalvan-admin-backend](../kalvan-admin-backend/README.md)
2. 下载idea插件[kalvan-gen](https://plugins.jetbrains.com/plugin/15002-kalvan-gen)
3. 参照官网文档运行[kalvan-admin](https://kalvan.store/docs/learn/kalvan-admin-cn)
  
# 前端开发
## 目录结构
```
├── public
│   └── logo.png             # LOGO
|   └── index.html           # Vue 入口模板
├── src
│   └── api                  # Api ajax 等
│       └── xxxx             # 模块
│           └── 子模块.js     # 子模块
│   └── assets               # 本地静态资源
│   └── components           # 业务通用组件
│   └── config               # 项目基础配置，包含路由，全局设置
│   └── core                 # 项目引导, 全局配置初始化，依赖包引入等
│   └── layouts              # 布局
│   └── locales              # 国际化资源
│   └── mock                 # 测试数据
│   └── router               # Vue-Router
│   └── store                # Vuex
│   └── utils                # 工具库
│   └── views                # 业务页面入口和常用模板
|       └── user             # 用户登录注册模块
|       └── system           # 系统管理模块         
|       └── xxxx             # 模块
|           └── xxxx         # 菜单
|               └── index    # 信息查询入口
|               └── save     # 信息新增或修改组件
|               └── info     # 详情查看
│   └── App.vue              # Vue 模板入口
│   └── global.less          # 全局样式
│   └── main.js              # Vue 入口 JS
│   └── permission.js        # 路由守卫(路由权限控制)
├── tests                    # 测试工具
├── README.md
└── package.json
```

## 已集成功能
**用户登录**
![](https://note.youdao.com/yws/api/personal/file/WEBc45bff8c280d8d99297bab76dc40c34a?method=download&shareKey=7759b0f54e90123a71101088dbbceda1)

**管理员管理**
![](https://note.youdao.com/yws/api/personal/file/WEBe20e102a85e80686d1a0081332f1fe86?method=download&shareKey=7759b0f54e90123a71101088dbbceda1)

**角色管理**
![](https://note.youdao.com/yws/api/personal/file/WEBd5f4e081abb9b80bd8e817577e62bd23?method=download&shareKey=7759b0f54e90123a71101088dbbceda1)

**菜单管理**
![](https://note.youdao.com/yws/api/personal/file/WEB65f24a5fc8b8a3019c17ed210d313bf7?method=download&shareKey=7759b0f54e90123a71101088dbbceda1)

**字典管理**
![](https://note.youdao.com/yws/api/personal/file/WEB9091773013e29268b7aef4c8a09a6bb5?method=download&shareKey=7759b0f54e90123a71101088dbbceda1)

**日志管理**
![](https://note.youdao.com/yws/api/personal/file/WEBfbcdb24b03577d5474ce15c2ef0c7e9e?method=download&shareKey=7759b0f54e90123a71101088dbbceda1)

**插件自动生成demo**
![](https://note.youdao.com/yws/api/personal/file/WEB97556f28d286e99824d62da98737667c?method=download&shareKey=7759b0f54e90123a71101088dbbceda1)

## 插件生成代码范围
- 分页查询
- 全局工具条
- 行工具条
- 批量删除
- 批量下载
- 批量导入
- 新增
- 单行详情
- 单行编辑
- 单行删除
- 单笔审核
- 批量审核
- 条件字典转换
- 表格字典转换
- 数字金额单位转换
- 表单验证
- 汇总数据行
- 按钮权限控制
- 脱敏转换(后台)
- 最小数据更新

# 其他说明

## 多页面展示
```
multiTab: true,
```

## mock
```
src/main.js
.env.development 参数 VUE_APP_API_BASE_URL
```
## 登录
```
src/views/user/Login.vue
src/api/login.js
src/store/modules/user.js
```
## 路由
```
src/permission.js
src/store/index.js
src/store/modules/async-router.js
src/router/generator-routers.js
```

- 关闭 Eslint (不推荐) 移除 `package.json` 中 `eslintConfig` 整个节点代码, `vue.config.js` 下的 `lintOnSave` 值改为 `false`
- 组件按需加载 `/src/main.js` L14 相关代码 `import './core/lazy_use'` / `import './core/use'` 
- I18n: [多语言支持 (@musnow)](./src/locales/index.js)
- [修改 Ant Design 配色 (@kokoroli)](https://github.com/kokoroli/antd-awesome/blob/master/docs/Ant_Design_%E6%A0%B7%E5%BC%8F%E8%A6%86%E7%9B%96.md)
- ***特别感谢***<a href="https://www.antdv.com/docs/vue/introduce-cn/" target="_blank">Ant Design of Vue</a> 和 <a href="https://pro.antdv.com" target="_blank">Ant Design of Vue Pro</a>
